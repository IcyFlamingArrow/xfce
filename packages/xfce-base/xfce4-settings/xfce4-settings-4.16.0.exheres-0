# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2011 Mickaël Guérin <kael@crocobox.org>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'xfce4-session-4.4.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require xfce

SUMMARY="XFCE Settings Manager"
HOMEPAGE="https://docs.xfce.org/xfce/${PN}/"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    colord
    cursor-themes [[ description = [ Enable Cursor themes support ] ]]
    debug
    keyboard-layout [[ description = [ Enable Keyboard layout selection ] ]]
    libinput [[ description = [ Enable touchpad support with libinput ] ]]
    libnotify [[ description = [ Enable notification support ] ]]
    sound-settings [[ description = [ Enable sound XSETTINGS in GUI ] ]]
    upower
"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        dev-lang/python:*[>=3.0] [[ note = xfce4-compose-mail ]]
        dev-libs/glib:2[>=2.24.0]
        media-libs/fontconfig[>=2.6.0]
        sys-apps/pnp-data
        x11-libs/gtk+:3[>=3.20.0]
        x11-libs/libX11[>=1.0.0]
        x11-libs/libXi[>=1.2.0]
        x11-libs/libXrandr[>=1.2.0]
        xfce-base/exo[>=0.11.0]
        xfce-base/garcon[>=0.1.10]
        xfce-base/libxfce4ui[>=4.15.1]
        xfce-base/libxfce4util[>=4.15.2]
        xfce-base/xfconf[>=4.13.0]
        colord? ( sys-apps/colord[>=1.0.2] )
        keyboard-layout? ( x11-libs/libxklavier )
        libinput? ( x11-drivers/xf86-input-libinput[>=0.6.0] )
        libnotify? ( x11-libs/libnotify[>=0.1.3] )
        sound-settings? ( media-libs/libcanberra )
        upower? ( sys-apps/upower[>=0.9.8][gobject-introspection] )
    run:
        gnome-desktop/gsettings-desktop-schemas
    recommendation:
        cursor-themes? ( x11-data/xcursor-themes )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-pluggable-dialogs
    --with-pnp-ids-path=/usr/share/misc/pnp.ids
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    colord
    "cursor-themes xcursor"
    debug
    "keyboard-layout libxklavier"
    "libinput xorg-libinput"
    libnotify
    sound-settings
    "upower upower-glib"
)


# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'xfwm4-4.4.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require xfce

SUMMARY="XFCE Window Manager"
HOMEPAGE="https://docs.xfce.org/xfce/${PN}/"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    startup-notification
    xcomposite
"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2[>=2.24]
        dev-libs/libepoxy[>=1.0]
        gnome-desktop/libwnck:3.0[>=3.14]
        sys-apps/dbus[>=1.0.0]
        x11-libs/gtk+:3[>=3.22.0]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXinerama
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXres
        xfce-base/libxfce4ui[>=4.12.0]
        xfce-base/libxfce4util[>=4.8.0]
        xfce-base/xfconf[>=4.13.0]
        startup-notification? ( x11-libs/startup-notification[>=0.5] )
        xcomposite? ( x11-libs/libXcomposite[>=0.2] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-render
    --enable-xrandr
    --enable-xsync
    --disable-xi2
    --disable-xpresent
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    startup-notification
    'xcomposite compositor'
)
